# personio-test

Test developed for the Front-End opportunity at Personio

To run install the dependencies with `yarn` and `yarn start` to run the app.
To run the test suite, use `yarn test` and to build use `yarn build`

The system was developed with:

```
styled-component
eslint
prettier
parcel
redux
redux-saga
stylelint
babel
icon awesome
jest
ezyme
```
