import { combineReducers } from 'redux';

const initialState = { data: undefined };

const AppStore = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_DATA':
      return {
        isLoading: true
      };
    case 'GET_DATA_SUCCESS':
      return {
        data: action.payload
      };
    case 'GET_DATA_ERROR':
      return {
        error: {
          data: action.payload
        }
      };
    case 'UPDATE_DATA':
      return {
        isLoading: true
      };
    case 'UPDATE_DATA_SUCCESS':
      return {
        data: action.payload
      };
    case 'UPDATE_DATA_ERROR':
      return {
        error: {
          data: action.payload
        }
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  app: AppStore
});

export default rootReducer;
