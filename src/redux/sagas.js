import { delay } from 'redux-saga';
import { takeLatest, put } from 'redux-saga/effects';
import axios from 'axios';

const API = 'https://json-server-api.herokuapp.com/personio';

function* fetchData() {
  try {
    const response = yield axios.get(API);
    if (response.data.length > 0)
      yield put({ type: 'GET_DATA_SUCCESS', payload: response.data[0] });
  } catch (e) {
    console.error(e);
    yield put({ type: 'GET_DATA_ERROR', payload: "There's a problem to fetch the data" });
  } finally {
    yield delay(100000);
  }
}

function* rootSaga() {
  yield takeLatest('GET_DATA', fetchData);
}

export default rootSaga;
