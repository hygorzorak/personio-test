import React from 'react';
import { connect } from 'react-redux';

import Header from '../components/Header';
import Wrapper from '../components/Wrapper';
import Board from '../components/Board';
import Upload from '../components/Upload';

const actions = {
  init() {
    return {
      appName: 'Personio HR',
      appDescription: 'HR Manager System',
      jsonFile: undefined
    };
  },
  updateJsonFile(jsonFile) {
    return { jsonFile };
  }
};

class App extends React.Component {
  state = actions.init();

  componentDidMount() {
    const { getData } = this.props;
    getData();
  }

  componentDidUpdate(prevProps) {
    const nextProps = this.props;

    if (prevProps.data !== nextProps.data) {
      const { data } = this.props;
      this.handleUploadJson(data);
    }
  }

  onUploadJson = e => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = e => {
      const { result } = e.target;
      this.handleUploadJson(JSON.parse(result));
    };
    reader.readAsText(file);
  };

  handleUploadJson = jsonFile => this.setState(actions.updateJsonFile(jsonFile));

  render() {
    const { appName, jsonFile } = this.state;

    return (
      <div>
        <Header type="h2" title={appName} />
        <Wrapper center>
          <Upload onChange={this.onUploadJson} />
        </Wrapper>
        <Wrapper>{jsonFile ? <Board data={jsonFile} /> : 'Loading...'}</Wrapper>
      </div>
    );
  }
}

const mapStateToProps = ({ app }) => ({
  data: app.data
});

const mapDispatchToProps = dispatch => ({
  getData: () => dispatch({ type: 'GET_DATA' })
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
