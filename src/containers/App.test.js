import React from 'react';
import { connect } from 'react-redux';
import { shallow } from 'enzyme';

import App from './App';

const ConectApp = () => connect()(App);

describe('>>> App - Shallow Render React Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ConectApp />);
  });

  it('---> render the component', () => {
    expect(wrapper.length).toEqual(1);
  });
});
