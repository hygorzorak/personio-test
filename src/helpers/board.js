export default {
  formatData(data) {
    const dataFormated = {};
    const firstLevelKey = Object.keys(data).map(key => key);
    dataFormated['first'] = {
      id: firstLevelKey[0],
      level: 'first',
      name: firstLevelKey[0],
      position: data[`${firstLevelKey}`].position,
      employees: data[`${firstLevelKey}`].employees
    };
    if (dataFormated['first'].employees.length > 0) {
      const dataSecondLevel = [];
      dataFormated['first'].employees.map((secondLevel, index) => {
        const secondLevelKey = Object.keys(dataFormated['first'].employees[index]).map(key => key);
        const formated = {
          key: firstLevelKey[0],
          id: secondLevelKey[0],
          level: 'second',
          name: secondLevelKey[0],
          position: secondLevel[`${secondLevelKey}`].position,
          employees: secondLevel[`${secondLevelKey}`].employees
        };
        dataSecondLevel.push(formated);
      });
      dataFormated['second'] = dataSecondLevel;
    }
    if (dataFormated['second'].length > 0) {
      const dataThirdLevel = [];
      let formated = {};
      dataFormated['second'].map(el => {
        el.employees.map((thirdLevel, index) => {
          const thirdLevelKey = Object.keys(thirdLevel).map(key => key);
          formated = {
            key: el.name,
            id: thirdLevelKey[0],
            level: 'third',
            name: thirdLevelKey[0],
            position: thirdLevel[`${thirdLevelKey}`].position,
            employees: thirdLevel[`${thirdLevelKey}`].employees
          };
          dataThirdLevel.push(formated);
        });
      });
      dataFormated['third'] = dataThirdLevel;
    }
    if (dataFormated['third'].length > 0) {
      const dataFourthLevel = [];
      let formated = {};
      dataFormated['third'].map(el => {
        el.employees.map((fourthLevel, index) => {
          const fourthLevelKey = Object.keys(fourthLevel).map(key => key);
          formated = {
            key: el.name,
            id: fourthLevelKey[0],
            level: 'fourth',
            name: fourthLevelKey[0],
            position: fourthLevel[`${fourthLevelKey}`].position,
            employees: fourthLevel[`${fourthLevelKey}`].employees
          };
          dataFourthLevel.push(formated);
        });
      });
      dataFormated['fourth'] = dataFourthLevel;
    }

    return dataFormated;
  },
  reorder: (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  },
  getListStyle: isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    padding: 10,
    width: 200,
    height: '100%'
  })
};
