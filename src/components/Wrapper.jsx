import React from 'react';
import styled from 'styled-components';

const WrapperComponent = ({ children, center }) => <Wrapper center={center}>{children}</Wrapper>;

const Wrapper = styled.div`
  margin: 10px;
  padding: 10px;
  width: 95%;
  display: flex;
  justify-content: ${({ center }) => (center ? 'center' : 'space-around')};
  align-items: ${({ center }) => (center ? 'center' : '')};
  max-width: 900px;
  margin: 0 auto;
`;

export default WrapperComponent;
