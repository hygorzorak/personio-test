import React from 'react';
import { shallow } from 'enzyme';

import Header from './Header';

describe('>>> Header - Shallow Render React Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Header title="Header" type="test" />);
  });

  it('---> render the component', () => {
    expect(wrapper.length).toEqual(1);
  });
});
