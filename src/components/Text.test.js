import React from 'react';
import { shallow } from 'enzyme';

import Text from './Text';

describe('>>> Text - Shallow Render React Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Text>Text test</Text>);
  });

  it('---> render the component', () => {
    expect(wrapper.length).toEqual(1);
  });
});
