import React from 'react';
import { shallow } from 'enzyme';

import Card from './Card';

describe('>>> Card - Shallow Render React Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Card name="Card" career="test" />);
  });

  it('---> render the component', () => {
    expect(wrapper.length).toEqual(1);
  });
});
