import React from 'react';
import { shallow } from 'enzyme';

import Title from './Title';

describe('>>> Title - Shallow Render React Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Title type="test">Title test</Title>);
  });

  it('---> render the component', () => {
    expect(wrapper.length).toEqual(1);
  });
});
