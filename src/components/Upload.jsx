import React from 'react';
import styled from 'styled-components';

const UploadComponent = ({ onChange }) => (
  <Wrapper>
    JSON file: <Upload type="file" onChange={onChange} accept=".json,application/json" />
  </Wrapper>
);

const Wrapper = styled.div``;
const Upload = styled.input``;

export default UploadComponent;
