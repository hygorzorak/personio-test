import React from 'react';
import styled from 'styled-components';

import Text from './Text';

const CardComponent = ({ name, career, onClick = () => null, selected = false }) => (
  <Card onClick={onClick} selected={selected}>
    <Text fontSize="10px">
      <b>{name}</b>
    </Text>
    <Text fontSize="10px">{career}</Text>
  </Card>
);

const Card = styled.div`
  -webkit-box-shadow: 0px 0px 10px -1px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 10px -1px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 10px -1px rgba(0, 0, 0, 0.75);
  background: ${({ selected }) => (selected ? '#ffffff' : '#f1f1f1')};
  border-radius: 5px;
  padding: 15px 30px;
  min-width: 100px;
  cursor: pointer;
  margin: 20px 0;
  text-transform: uppercase;
`;

export default CardComponent;
