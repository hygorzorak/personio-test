import React from 'react';
import { shallow } from 'enzyme';

import Upload from './Upload';

describe('>>> Upload - Shallow Render React Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Upload onChange={() => {}} />);
  });

  it('---> render the component', () => {
    expect(wrapper.length).toEqual(1);
  });
});
