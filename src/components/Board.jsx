import React from 'react';
import styled from 'styled-components';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import Card from './Card';

import helpers from '../helpers/board';

const actions = {
  init() {
    return {
      selectedFirstLevel: 'Jonas',
      selectedSecondLevel: 'Sophie',
      selectedThirdLevel: 'Nick',
      selectedFourthLevel: '',
      firstLevelContent: [],
      secondLevelContent: [],
      thirdLevelContent: [],
      fourthLevelContent: [],
      showModal: false
    };
  },
  updateSelectedFirstLevel(selectedFirstLevel) {
    return {
      selectedFirstLevel
    };
  },
  updateSelectedSecondLevel(selectedSecondLevel) {
    return {
      selectedSecondLevel
    };
  },
  updateSelectedThirdLevel(selectedThirdLevel) {
    return {
      selectedThirdLevel
    };
  },
  updateContent(key, content) {
    return {
      [key]: content
    };
  },
  updateModal(showModal) {
    return {
      showModal
    };
  }
};

class BoardComponent extends React.Component {
  state = actions.init();

  componentDidMount() {
    this.onLoadJson();
  }

  componentDidUpdate(prevProps) {
    const nextProps = this.props;
    if (prevProps.data !== nextProps.data) {
      this.onLoadJson();
    }
  }

  onLoadJson = () => {
    const { data } = this.props;
    const { selectedSecondLevel, selectedThirdLevel } = this.state;

    if (data) {
      const dataFormated = helpers.formatData(data);

      const firstLevelContent = dataFormated['first'] ? [dataFormated['first']] : [];
      const secondLevelContent = [dataFormated['second']] ? dataFormated['second'] : [];
      const thirdLevelContent = dataFormated['third']
        ? dataFormated['third'].filter(el => el.key === selectedSecondLevel)
        : [];
      const fourthLevelContent = dataFormated['fourth']
        ? dataFormated['fourth'].filter(el => el.key === selectedThirdLevel)
        : [];

      this.handleLoadData(
        firstLevelContent,
        secondLevelContent,
        thirdLevelContent,
        fourthLevelContent
      );
    }
  };

  onDragEnd = result => {
    if (!result.destination) return;
    if (result.source.droppableId === result.destination.droppableId) {
      const originalList = this.state[`${result.source.droppableId}LevelContent`];
      const updatedList = helpers.reorder(
        originalList,
        result.source.index,
        result.destination.index
      );
      return this.setState(
        actions.updateContent(`${result.source.droppableId}LevelContent`, updatedList)
      );
    }
    const original = this.state[`${result.source.droppableId}LevelContent`];
    const resultElement = original.filter(el => el.name === result.draggableId);
    const originalEdited = original.filter(el => el.name !== result.draggableId);
    if (
      (result.source.droppableId === 'first' && originalEdited.length === 0) ||
      resultElement[0].employees.length !== 0
    ) {
      return this.handleOpenModal();
    }
    const target = this.state[`${result.destination.droppableId}LevelContent`];

    let selectedKey = '';
    result.destination.droppableId === 'second' && (selectedKey = this.state.selectedFirstLevel);
    result.destination.droppableId === 'third' && (selectedKey = this.state.selectedSecondLevel);
    result.destination.droppableId === 'fourth' && (selectedKey = this.state.selectedThirdLevel);

    target.push({
      ...resultElement[0],
      key: selectedKey,
      level: result.destination.droppableId
    });

    if (result.source.droppableId === 'second') {
      const prev = this.state[`firstLevelContent`].filter(el => el.name === resultElement[0].key);
      prev[0].employees.map(el => {
        const key = Object.keys(el).map(key => key);
        if (key[0] === resultElement[0].name) {
          prev[0].employees.splice(prev[0].employees.indexOf(key), 1);

          key[0] === result.draggableId &&
            prev.push({
              key:
                result.destination.droppableId === 'third'
                  ? this.state.selectedSecondLevel
                  : result.destination.droppableId === 'fourth'
                    ? this.state.selectedThirdLevel
                    : '',
              id: key[0],
              level: result.destination.droppableId,
              name: key[0],
              position: el[key[0]].position,
              employees: el[key[0]].employees
            });

          if (result.destination.droppableId === 'first') {
            this.setState(actions.updateContent(`firstLevelContent`, prev));
          }
          if (result.destination.droppableId === 'third') {
            this.setState(actions.updateContent(`thirdLevelContent`, prev));
            const newState = this.state[`secondLevelContent`];
            newState.map(
              second => second.id === this.state.selectedThirdLevel && second.employees.push(el)
            );
          }
          if (result.destination.droppableId === 'fourth') {
            this.setState(actions.updateContent(`fourthLevelContent`, prev));
            const newState = this.state[`thirdLevelContent`];
            newState.map(
              second => second.id === this.state.selectedFourthLevel && second.employees.push(el)
            );
          }
        }
      });
    }

    if (result.source.droppableId === 'third') {
      const prev = this.state[`secondLevelContent`].filter(el => el.name === resultElement[0].key);
      prev[0].employees.map(el => {
        const key = Object.keys(el).map(key => key);
        if (key[0] === resultElement[0].name) {
          prev[0].employees.splice(prev[0].employees.indexOf(key), 1);

          key[0] === result.draggableId &&
            prev.push({
              key:
                result.destination.droppableId === 'second'
                  ? this.state.selectedFirstLevel
                  : result.destination.droppableId === 'fourth'
                    ? this.state.selectedThirdLevel
                    : '',
              id: key[0],
              level: result.destination.droppableId,
              name: key[0],
              position: el[key[0]].position,
              employees: el[key[0]].employees
            });

          if (result.destination.droppableId === 'first') {
            this.setState(actions.updateContent(`firstLevelContent`, prev));
          }
          if (result.destination.droppableId === 'second') {
            this.setState(actions.updateContent(`secondLevelContent`, prev));
            const newState = this.state[`firstLevelContent`];
            newState.map(
              second => second.id === this.state.selectedSecondLevel && second.employees.push(el)
            );
          }
          if (result.destination.droppableId === 'fourth') {
            this.setState(actions.updateContent(`fourthLevelContent`, prev));
            const newState = this.state[`thirdLevelContent`];
            newState.map(
              second => second.id === this.state.selectedFourthLevel && second.employees.push(el)
            );
          }
        }
      });
    }

    if (result.source.droppableId === 'fourth') {
      const prev = this.state[`thirdLevelContent`].filter(el => el.name === resultElement[0].key);
      prev[0].employees.map(el => {
        const key = Object.keys(el).map(key => key);
        if (key[0] === resultElement[0].name) {
          prev[0].employees.splice(prev[0].employees.indexOf(key), 1);

          key[0] === result.draggableId &&
            prev.push({
              key:
                result.destination.droppableId === 'second'
                  ? this.state.selectedFirstLevel
                  : result.destination.droppableId === 'third'
                    ? this.state.selectedSecondLevel
                    : '',
              id: key[0],
              level: result.destination.droppableId,
              name: key[0],
              position: el[key[0]].position,
              employees: el[key[0]].employees
            });

          if (result.destination.droppableId === 'first') {
            this.setState(actions.updateContent(`firstLevelContent`, prev));
          }
          if (result.destination.droppableId === 'second') {
            this.setState(actions.updateContent(`secondLevelContent`, prev));
            const newState = this.state[`firstLevelContent`];
            newState.map(
              second => second.id === this.state.selectedSecondLevel && second.employees.push(el)
            );
          }
          if (result.destination.droppableId === 'third') {
            this.setState(actions.updateContent(`thirdLevelContent`, prev));
            const newState = this.state[`secondLevelContent`];
            newState.map(
              second => second.id === this.state.selectedThirdLevel && second.employees.push(el)
            );
          }
        }
      });
    }

    this.setState(
      actions.updateContent(`${result.source.droppableId}LevelContent`, originalEdited)
    );

    this.setState(actions.updateContent(`${result.destination.droppableId}LevelContent`, target));
  };

  handleLoadData = (
    firstLevelContent,
    secondLevelContent,
    thirdLevelContent,
    fourthLevelContent
  ) => {
    this.setState(actions.updateContent('firstLevelContent', firstLevelContent));
    this.setState(actions.updateContent('secondLevelContent', secondLevelContent));
    this.setState(actions.updateContent('thirdLevelContent', thirdLevelContent));
    this.setState(actions.updateContent('fourthLevelContent', fourthLevelContent));
  };

  handleUpdateSelected = (level, value) => {
    if (level === 'first') {
      if (this.state.selectedFirstLevel === value.name) {
        this.setState(actions.updateSelectedFirstLevel(''));
      } else {
        this.setState(actions.updateSelectedFirstLevel(value.name));
      }
      this.setState(actions.updateSelectedSecondLevel(''));
      this.setState(actions.updateSelectedThirdLevel(''));
    }
    if (level === 'second') {
      if (this.state.selectedSecondLevel === value.name) {
        this.setState(actions.updateSelectedSecondLevel(''));
      } else {
        this.setState(actions.updateSelectedSecondLevel(value.name));
      }
      this.setState(actions.updateSelectedThirdLevel(''));
    }
    if (level === 'third') {
      if (this.state.selectedThirdLevel === value.name) {
        this.setState(actions.updateSelectedThirdLevel(''));
      } else {
        this.setState(actions.updateSelectedThirdLevel(value.name));
      }
    }
  };

  handleOpenModal = () => this.setState(actions.updateModal(true));

  handleCloseModal = () => this.setState(actions.updateModal(false));

  render() {
    const { data } = this.props;
    const {
      selectedFirstLevel,
      selectedSecondLevel,
      selectedThirdLevel,
      firstLevelContent,
      secondLevelContent,
      thirdLevelContent,
      fourthLevelContent,
      showModal
    } = this.state;

    return (
      <Board>
        {!data && <p>There's no data to show</p>}
        {data && (
          <DragDropContext onDragEnd={result => this.onDragEnd(result)}>
            <Col>
              <Droppable droppableId="first">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={helpers.getListStyle(snapshot.isDraggingOver)}
                  >
                    <h5>FIRST LEVEL: </h5>
                    {firstLevelContent.map((item, index) => (
                      <Draggable key={item.id} draggableId={item.id} index={index}>
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <Card
                              name={item.name}
                              career={item.position}
                              onClick={() => this.handleUpdateSelected('first', item)}
                              selected
                            />
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </Col>

            <Col>
              <Droppable droppableId="second">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={helpers.getListStyle(snapshot.isDraggingOver)}
                  >
                    <h5>SECOND LEVEL: </h5>
                    {secondLevelContent.map((item, index) => (
                      <Draggable key={item.id} draggableId={item.id} index={index}>
                        {provided => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            {item.key === selectedFirstLevel && (
                              <Card
                                key={item.name}
                                name={item.name}
                                career={item.position}
                                onClick={() => this.handleUpdateSelected('second', item)}
                                selected={item.name === selectedSecondLevel}
                              />
                            )}
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </Col>

            <Col>
              <Droppable droppableId="third">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={helpers.getListStyle(snapshot.isDraggingOver)}
                  >
                    <h5>THIRD LEVEL: </h5>
                    {thirdLevelContent.map((item, index) => (
                      <Draggable key={item.id} draggableId={item.id} index={index}>
                        {provided => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            {item.key === selectedSecondLevel && (
                              <Card
                                key={item.name}
                                name={item.name}
                                career={item.position}
                                onClick={() => this.handleUpdateSelected('third', item)}
                                selected={item.name === selectedThirdLevel}
                              />
                            )}
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </Col>

            <Col>
              <Droppable droppableId="fourth">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={helpers.getListStyle(snapshot.isDraggingOver)}
                  >
                    <h5>FOURTH LEVEL: </h5>
                    {fourthLevelContent.map((item, index) => (
                      <Draggable key={item.id} draggableId={item.id} index={index}>
                        {provided => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            {item.key === selectedThirdLevel && (
                              <Card
                                key={item.name}
                                name={item.name}
                                career={item.position}
                                selected
                              />
                            )}
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </Col>
          </DragDropContext>
        )}
        {showModal && <ModalComponent onClick={this.handleCloseModal} />}
      </Board>
    );
  }
}

const ModalComponent = ({ onClick }) => (
  <Modal>
    You cant't remove all elements from a list with employees!
    <Button onClick={onClick}>OK</Button>
  </Modal>
);

const Modal = styled.div`
  position: absolute;
  padding: 30px 40px;
  background: #333333;
  box-shadow: 5px 5px 25px 5px rgba(0, 0, 0, 0.75);
  border-radius: 5px;
  color: #fff;
`;
const Button = styled.button`
  display: block;
  margin: 20px auto;
  padding: 5px 15px;
  border-radius: 5px;
  cursor: pointer;
`;
const Board = styled.div`
  display: flex;
  min-height: 75vh;
  width: 100%;
  justify-content: space-around;
`;
const Col = styled.div`
  height: 100%;
`;

export default BoardComponent;
