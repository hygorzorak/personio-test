import React from 'react';
import { shallow } from 'enzyme';

import Board from './Board';

describe('>>> Board - Shallow Render React Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Board />);
  });

  it('---> render the component', () => {
    expect(wrapper.length).toEqual(1);
  });
});
