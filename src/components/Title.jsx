import React from 'react';
import styled from 'styled-components';

const TitleComponent = ({ type, children }) => (
  <Title>
    {type === 'h1' && <h1>{children}</h1>}
    {type === 'h2' && <h2>{children}</h2>}
    {type === 'h3' && <h3>{children}</h3>}
    {type === 'h4' && <h4>{children}</h4>}
    {type === 'h5' && <h5>{children}</h5>}
  </Title>
);

const Title = styled.div`
  font-family: sans-serif;
  text-transform: uppercase;
  line-height: 1.5em;
`;

export default TitleComponent;
