import React from 'react';
import { shallow } from 'enzyme';

import Wrapper from './Wrapper';

describe('>>> Wrapper - Shallow Render React Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Wrapper>Wrapper test</Wrapper>);
  });

  it('---> render the component', () => {
    expect(wrapper.length).toEqual(1);
  });
});
