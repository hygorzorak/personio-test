import React from 'react';
import styled from 'styled-components';

const TextComponent = ({ children, fontSize = '14px' }) => (
  <Text fontSize={fontSize}>{children}</Text>
);

const Text = styled.p`
  font-size: ${({ fontSize }) => fontSize};
  margin: 0;
  padding: 0;
`;

export default TextComponent;
