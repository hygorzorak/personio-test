import React from 'react';
import styled from 'styled-components';

import Title from './Title';

const HeaderComponent = ({ type, title }) => (
  <Header>
    <Title type={type}>{title}</Title>
  </Header>
);

const Header = styled.div`
  background: #333;
  color: #fff;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  -webkit-box-shadow: -1px -1px 18px 5px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: -1px -1px 18px 5px rgba(0, 0, 0, 0.75);
  box-shadow: -1px -1px 18px 5px rgba(0, 0, 0, 0.75);
`;

export default HeaderComponent;
