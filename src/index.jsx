import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { injectGlobal } from 'styled-components';

import 'typeface-roboto';

import store from './redux/setup';
import App from './containers/App';

injectGlobal`
*,
*::after,
*::before {
  box-sizing: border-box;
}
html,
body {
  margin: 0;
  padding: 0;
  min-height: 100%;
}
body {
  background-color: #ecf0f1;
  color: #333;
  font-family: sans-serif;
  font-size: 14px;
  line-height: 1.25rem;
}
`;

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
